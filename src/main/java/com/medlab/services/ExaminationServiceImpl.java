package com.medlab.services;

import com.medlab.repositories.contracts.ExaminationRepository;
import com.medlab.services.contracts.ExaminationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExaminationServiceImpl implements ExaminationService {
    private final ExaminationRepository examinationRepository;

    @Autowired
    ExaminationServiceImpl(ExaminationRepository examinationRepository) {
        this.examinationRepository = examinationRepository;
    }
}
