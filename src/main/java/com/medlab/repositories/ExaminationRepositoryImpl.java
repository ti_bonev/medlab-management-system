package com.medlab.repositories;

import com.medlab.repositories.contracts.ExaminationRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ExaminationRepositoryImpl implements ExaminationRepository {
    private final SessionFactory sessionFactory;
    @Autowired
    public ExaminationRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
