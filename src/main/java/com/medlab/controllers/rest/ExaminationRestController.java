package com.medlab.controllers.rest;

import com.medlab.services.contracts.ExaminationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/lab/examinations")
public class ExaminationRestController {
    private final ExaminationService examinationService;

    @Autowired
    public ExaminationRestController(ExaminationService examinationService) {
        this.examinationService = examinationService;
    }
}
