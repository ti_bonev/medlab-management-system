package com.medlab.repositories.contracts;

import com.medlab.models.User;

import java.util.List;

public interface UserRepository {
    List<User> getAll();
}
