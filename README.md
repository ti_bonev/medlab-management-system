# MedLAb = Medical Laboratory Management System

by Todor Bonev


## Project Details

MedLab is a web application... 

## Quick Overview

-  Check the ui-present.gif file with screenshots of the three UI interfaces.
-  Check the schema.jpg in ../db/ to view db structure

## Configure and run the project on your device

The project is developed on Java 17 with Spring MVC, MySQL, and Thymeleaf. The database is stored locally. To run the project follow these steps:

-  Choose Clone / “Clone with HTTPS” or download the source code as an archive
-  Open project in IDE
-  Create your local application.properties file in ..\SmartGarage\src\main\resources directory. Use your local server credentials to establish a connection. To try “send e-mail option you can configure a connection with your own e-mail host, or use current with account in abv.bg. File content is the following:

```
server.error.include-stacktrace=never
server.error.include-message=always
# DataBase Configuration
database.url=jdbc:mysql://localhost:3306/smart_garage
database.username=<TYPE YOUR USERNAME>
database.password=<TYPE YOUR PASSWORD>
# Mail Server Configuration for abv.bg
spring.mail.host=smtp.abv.bg
spring.mail.port=465
spring.mail.username=mighty_it@abv.bg<USE YOUR E-MAIL>
spring.mail.password=<TYPE YOUR PASSWORD>
spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.starttls.enable=true
mail.smtp.debug=true
# Swagger configuration
spring.mvc.pathmatch.matching-strategy=ANT_PATH_MATCHER
```

-  Run script “create_schema_insert_data.sql” from ..\db to create database schema and fill data on your local server
-  Run the project
-  Load http://localhost:8080/ in your browser
-  At the footer of admin panel, on dev icon you can find link to Swagger documentation
-  Enjoy!

## About

The project started on 29/08/2023.
