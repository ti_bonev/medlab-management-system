package com.medlab.models;

import jakarta.persistence.*;

@Entity
@Table(name = "examination")
public class Examination {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "title")
    private String title;
    @Column(name = "test_result")
    private int result;
    @Column(name = "test_units")
    private String units;
    @Column(name = "test_method")
    private String method;
    @Column(name = "reference_min")
    private int referenceMin;
    @Column(name = "reference_max")
    private int referenceMax;
    @Column(name = "price")
    private int price;

//    @ManyToOne
//    @JoinColumn("name =  "examination_type_id")
//    private int typeId;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getReferenceMin() {
        return referenceMin;
    }

    public void setReferenceMin(int referenceMin) {
        this.referenceMin = referenceMin;
    }

    public int getReferenceMax() {
        return referenceMax;
    }

    public void setReferenceMax(int referenceMax) {
        this.referenceMax = referenceMax;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
