package com.medlab.services.contracts;

import com.medlab.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll();
}
